# -*- coding: utf-8 -*-
import urllib.request
import json
import api

class ApiCaller:
  def __init__(self):
    self.responses = {}
    self.responses["constCompanies"] = api.Get("ConstructionCompany").format(["companyName","satisfactions"], "", False, 'constCompanies')
    self.responses["insuranceCompanies"] = api.Get("InsuranceCompany").format(["companyName"])
    self.responses["assessmentOrgs"] = api.Get("AssessmentOrganization").format(["companyId","companyName"])
    self.responses["contractors"] = api.Get("Contractor").format(["contractorId","Name"])
    self.responses["iotHouses"] = api.Get("IotHouse").format(["iotId","status"])
    self.responses["agreements"] = api.Get("Agreement").format(["policyId","contractor","iot","StartDate","endDate","address"], "policyId")
    self.responses["accidents"] = api.Get("Accident").format(["iotHouse","accrualDate"], "accrualDate", False)
    self.responses["incidents"] = api.Get("Incident").format(["incidentId","insurancepolicy", "status","accrualDate"], "incidentId", False)
    self.responses["investigations"] = api.Get("Investigate").format(["incident","incidentId","organization","status"], "incidentId")
    self.responses["ConstructionInformations"] = api.Get("ConstructionInformation").format(["constructInformId","status", "winningBid","constructionCompany"])
    self.responses["constructionBid"] = api.Get("ConstructionBid").format(["bidId","company","bidAmount","bidDate","winningBid"], "bidDate", False, 'ConstructionBid')
    self.responses["feedback"] = api.Get("Feedback").format(["incident","satisfaction"])
    self.dispData = {}

    #契約情報にIoTステータスを付加する
    for a in self.responses['agreements'].json_response:
      for i in self.responses['iotHouses'].json_response:
        if a.get('iot') == i.get('iotId'):
          a['status'] = i.get('status')
          if 'status' not in self.responses['agreements'].cols:
            self.responses['agreements'].cols.append('status')
          break

  def setIncidentInfo(self, companyName, incidentId):

    # 該当インシデントの情報を取得
    for i in self.responses["incidents"].json_response:
        if incidentId == i.get("incidentId"):
            self.dispData["incident"] = i
            break

    # 該当の契約情報を取得
    for a in self.responses["agreements"].json_response:
        if incidentId[:2] == a.get("policyId"):
            self.dispData["agreement"] = a
            break

    # 該当の工事情報を取得
    for c in self.responses["ConstructionInformations"].json_response:
        if incidentId == c.get("constructInformId")[3:]:
            self.dispData["bidStatus"] = c
            break
    print('self.dispData["bidStatus"]')
    print(self.dispData["bidStatus"])


    # (落札済の場合)、落札額を取得
    if self.dispData.get("bidStatus").get("winningBid") != "":
      for c in self.responses["constructionBid"].json_response:
        #if incidentId == c.get("bidId")[14:] and c.get("winningBid") == "●":
        if incidentId == c.get("bidId") and c.get("winningBid") == "●":
            self.dispData["successfulBid"] = c
            break
    print(self.dispData.get("successfulBid"))

    # 該当インシデントの自社の入札情報を取得
    for c in self.responses["constructionBid"].json_response:
        #if incidentId == c.get("bidId")[14:] and companyName == c.get("company").strip("建設"):
        if incidentId == c.get("bidId") and companyName == c.get("company").strip("建設"):
            self.dispData["myBid"] = c
            break

    # 未ビットではなく、落札していなければ入札済とする
    if self.dispData.get("myBid") != None and self.dispData.get("successfulBid") == None :
      self.dispData["bidStatus"]["status"] = "入札済"

class Admin(ApiCaller):
  def __init__(self):
    super().__init__()
    
class Sonpo(ApiCaller):
  def __init__(self):
    super().__init__()
    
class Investigate(ApiCaller):
  def __init__(self):
    super().__init__()
    
class Const(ApiCaller):
  def __init__(self, companyName="", incidentId=""):
    super().__init__()

    #Constトップへのアクセス
    self.addBidToIncidents(companyName)

    #Constトップへのアクセスの場合はここで処理終了
    if incidentId == "": return

    # /const/incidentIdの情報セット #
    self.setIncidentInfo(companyName, incidentId)

  def addBidToIncidents(self, companyName):
    #incidentsに入札状況を付加
    for i in self.responses["incidents"].json_response:

      #「入札待ち」「落札済」をセット
      for j in self.responses["ConstructionInformations"].json_response:
        if i.get("incidentId") == j.get("constructInformId")[3:]:
          i["status_const"] = j["status"]
          break

      print(i.get("status_const"))
      if i.get("status_const") == "落札済": continue

      #入札済の場合は「入札済」をセット
      for j in self.responses["constructionBid"].json_response:
        if i.get("incidentId") == j.get("bidId")[14:] and companyName == j.get("company").strip("建設"):
          i["status_const"] = "入札済"

      i["status_const"] = i.get("status_const", "募集なし")

    self.responses["incidents"].cols.append("status_const")

class Customer(ApiCaller):
  def __init__(self, companyName="", incidentId=""):
    super().__init__()

    print(self.responses['agreements'].json_response)
    print(companyName)

    #その人の契約のみ取得する
    for a in self.responses['agreements'].json_response:
      if a.get('contractor').startswith(companyName):
        self.dispData['myAgreement'] = a
        break

    print(self.dispData['myAgreement'])

    #incidentIdの一覧を取得
    self.dispData['myIncidents'] = []

    for i in self.responses['incidents'].json_response:
      if i.get('incidentId').startswith(self.dispData.get('myAgreement').get('policyId')):
        self.dispData['myIncidents'].append(i)

    print('myIncidents')
    print(self.dispData.get('myIncidents'))

    #Customerトップへのアクセスの場合はここで処理終了
    if incidentId == "": return

#    myAgreement = self.dispData.get('myAgreement')
#    incidentId = myAgreement.get('policyId') + "-" + myAgreement.get('iot')[3:]

    print("incidentId" + incidentId)

    # /customer/incidentIdの情報セット #
    self.setIncidentInfo(companyName, incidentId)

    for s in self.responses.get('feedback').json_response:
      if s.get('incident') == incidentId:
        self.dispData['score'] = s.get('satisfaction').get('score')

