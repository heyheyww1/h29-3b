# -*- coding: utf-8 -*-

import json
from datetime import datetime

class Format:
  def __init__(self, json_response, cols, sortKey, reverse, name=None):
    self.json_response = json_response
    self.cols = cols
    self.sortKey = sortKey
    self.reverse = reverse
    self.name = name
    self.format()

  def formatResources(self, data, col):
    prefixies = [
      "resource:org.itken.Contractor#",
      "resource:org.itken.IoTHouse#",
      "resource:org.itken.ConstructionCompany#",
      "resource:org.itken.Incident#",
      "resource:org.itken.InsurancePolicy#",
      "resource:org.itken.AssessmentOrganization#"]

    for p in prefixies:
      if data.get(col) == None: break
      if str(data.get(col)).startswith(p):
         data[col] = data.get(col)[len(p):]
         return

  def formatSatisfaction(self, data, col):
    str = ""
    if (len(data[col])) > 0:
      for i in range(data[col][-1]["score"]):
        str += "★"
    if str == "":
      str = "★★★"
    data[col] = str

  def formatStatus(self, data, col):
    if data[col] == "danger":
      data[col] = "危険"
    elif data[col] == "normal":
      data[col] = "正常"
    elif data[col] == "disabled":
      data[col] = "契約なし"
    elif data[col] == "onCorrespond":
      data[col] = "工事要"
    elif data[col] == "notCompatible":
      data[col] = "工事不要"
    elif data[col] == "bidding":
      data[col] = "入札待ち"
    elif data[col] == "done":
      data[col] = "落札済"

  def formatWinningBid(self, data, col):
    if data.get(col):
      data[col] = "●"
    else:
      data[col] = ""

  def formatCompany(self, data, col):
    if data.get(col) == "Company001":
      data[col] = "堀切建設"
    elif data.get(col) == "Company002":
      data[col] = "大金建設"
    elif data.get(col) == "Company003":
      data[col] = "山口建設"
    elif data.get(col) == None:
      data[col] = ""

  def formatOrg(self, data, col):
    if data[col] == "Org1":
      data[col] = "将彦調査"
    elif data[col] == "Org2":
      data[col] = "充調査"
    elif data[col] == "Org3":
      data[col] = "真美子調査"

  def formatContractor(self, data, col):
    if data[col] == "Person001":
      data[col] = "前田 将彦"
    elif data[col] == "Person002":
      data[col] = "横田 充"
    elif data[col] == "Person003":
      data[col] = "木下 真美子"

  def formatBidId(self, data, col):
    data[col] = data[col][14:]
    #data[col] = data[col]

  def format(self):
    for data in self.json_response:
      for col in self.cols:

        self.formatResources(data, col)

        #formatDate
        if col in ["accrualDate","bidDate"]:
          data[col] = data[col][:10] + " " + data[col][11:19]

        #formatDateTime
        elif col in ["StartDate","endDate"]:
          data[col] = data[col][:10]

        #満足度
        elif col == "satisfactions":
          self.formatSatisfaction(data, col)

        #ステータス
        elif col == "status":
          self.formatStatus(data, col)

        # 落札
        elif col == "winningBid":
          self.formatWinningBid(data, col)

        # 会社名
        elif col in ["company", "constructionCompany"]:
          self.formatCompany(data, col)

        # 調査機関名
        elif col in ["Org", "organization"]:
          self.formatOrg(data, col)

        # 契約者名
        elif col == "contractor":
          self.formatContractor(data, col)

        # bidId
        elif col == "bidId":
          self.formatBidId(data, col)

    # sortkey を指定されている場合はsortする
    if self.sortKey != "":
        print(self.sortKey)
        self.json_response = sorted(self.json_response,key=lambda x:x[self.sortKey],reverse=self.reverse)

    #配列の長さが6以上の場合は5以下にする
    if len(self.json_response) > 6:
      self.json_response = self.json_response[:5]

    #建設会社は3社のみ表示する 
    if self.name == 'constCompanies':
      self.json_response = self.json_response[:3]

