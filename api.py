# -*- coding: utf-8 -*-
import urllib.request
import json
import format

class Api:

  URL = "http://18.182.100.5:3000/api/org.itken."
# URL = "http://219.99.44.18:3001/api/org.itken."

  def __init__(self, api):
    self.api = api
    self.id = ""
    self.response = None

  def call(self):
    req = self.makeRequest()
    try:
      with urllib.request.urlopen(req) as res:
        body = res.read().decode('utf-8')
    except urllib.error.HTTPError as e:
      print(e.status)
      self.response = []       
    else:
      self.response = json.loads(body)

  def format(self, cols, sortKey="", reverse=False, name=""):
    return format.Format(self.response, cols, sortKey, reverse, name)

#Postクラス
class Post(Api):
  def __init__(self, api, data):
    super().__init__(api)
    self.data = data
    self.headers = { 'Content-Type': 'application/json', }
    self.call()

  def makeRequest(self):
    return urllib.request.Request(self.URL + self.api, json.dumps(self.data).encode(), self.headers)

#Getクラス
class Get(Api):
  def __init__(self, api, id=""):
    super().__init__(api)
    self.id = "/" + id
    self.call()

  def makeRequest(self):
    print(self.URL + self.api + self.id)
    return urllib.request.Request(self.URL + self.api + self.id)

  def getMax(self, col, prefix=""):
    # IDの最大値を取得
    ids = []
    for e in self.response:
        print("yes" + e[col])
        ids.append(int(e[col][len(prefix):]))
    if ids == []: return 0
    return max(ids)

  def getTransactionIdBy(self, col):
    return
