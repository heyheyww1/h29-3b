# -*- coding: utf-8 -*-

#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from flask import Flask, request, render_template
import urllib.request
import json
from datetime import datetime
import api
import apiCaller

app = Flask(__name__)
app.config['DEBUG'] = True

#地震を起こす
def earthquake(iotHouse):
    now_dt = datetime.now()
    now_str = now_dt.strftime("%Y-%m-%d") + "T" + now_dt.strftime("%H:%M:%S") + ".000Z"
    data = { "$class": "org.itken.Accident", "iotHouse": iotHouse, "accrualDate": now_str }
    api.Post("Accident", data)

def investigate(companyName, req_org, req_incident, status):
    # 1つの incident に対して、POSTされるごとに複数の incidentId が自動で増えていく    
    # Investigateの配列からユーザが指定したincidentを取得する
    res_investigate = api.Get("Investigate")
    for e in res_investigate.response:
        if e['incident'].endswith(req_incident) and e['organization'].endswith(req_org):
            return render_template("investigate/failed.html" 
                                   , companyName=companyName  
                                   , task="調査" 
                                   , errorMessage="そのインシデントは既に査定済です")

    max_incidentId = res_investigate.getMax("incidentId")

    data = { "$class": "org.itken.Investigate", 
             "incidentId": str(max_incidentId + 1),
             "status": status, 
             "incident": req_incident, 
             "organization": req_org 
           } 
    print(data)
    api.Post("Investigate", data)

def agreement(contractor, iot):
    # IDの最大値を取得
    agreements = api.Get("Agreement")

    data = {"$class": "org.itken.Agreement",
            "policyId": "p" + str(agreements.getMax('policyId','p') + 1),
            "contractor": contractor,
            "company": "ICompany",
            "iot": iot,
            "StartDate": "2018-08-10T08:09:28.376Z",
            "endDate": "2019-08-10T08:09:28.376Z",
            "address": "TOKYO",
            "timestamp": "2018-08-10T08:09:28.376Z"
            }

    api.Post("Agreement",data)

# index
@app.route("/")
def index():
    responses = apiCaller.Sonpo().responses
    return render_template("info/info.html", responses=responses)

# welcome
@app.route("/welcome", methods=['GET'])
def welcome():
    companyName = request.args.get('companyName',  default = '', type = str)
    return render_template("welcome.html",companyName=companyName)

# admin
@app.route("/admin")
def admin():
    responses = apiCaller.Admin().responses
    return render_template("/admin/info.html", responses=responses)

# セットアップボタン押下時の処理
@app.route("/SetUpDemo")
def setUpDemo():
    #setUpDemoを実行
    api.Post("SetUpDemo",{})

    #契約を2つ追加する
    agreement('Person001','IOT001')
    agreement('Person002','IOT002')

    return render_template("admin/complete.html", task="set up")

# 2回目のセットアップボタン押下時の処理
@app.route("/secondSetUp")
def secondSetUp():

    # IOT002の地震発生
    earthquake("IOT002")

    # 3社の調査を強制実行
    investigate("堀切", "Org1", "p2-001", "onCorrespond")
    investigate("大金", "Org2", "p2-001", "onCorrespond")
    investigate("山口", "Org3", "p2-001", "onCorrespond")

    return render_template("admin/complete.html", task="2週目 set up")

# 情報取得ボタン押下時の処理
@app.route("/info")
def info():
    responses = apiCaller.Sonpo().responses
    return render_template("info/info.html", responses=responses)
     
# 契約追加ボタン押下時の処理
@app.route("/addAgreements", methods=['POST'])
def addAgreements():

    agreement(request.form['contractor'], request.form['iot'])
    return render_template("sonpo/complete.html", task="契約追加")

# 地震ボタン押下時の処理
@app.route("/addAccidents", methods=['POST'])
def addAccidents():

    earthquake(request.form["iotHouse"])
    return render_template("/admin/complete.html", task="地震")

# 地震ボタン押下時の処理(from MESH)
@app.route("/addAccidentsByMESH", methods=['POST'])
def addAccidentsByMESH():

    # IOT002の地震発生
    earthquake("IOT001")

    return render_template("admin/complete.html", task="地震")

@app.route("/addInvestigates", methods=['POST'])
def addInvestigates():

    req_incident = request.form['incident']
    req_org = request.form['organization']
    companyName = request.form['companyName']
    status = request.form['status']

    investigate(companyName, req_org, req_incident, status)

    return render_template("investigate/complete.html", companyName=companyName, task="調査")

@app.route("/bid", methods=['POST'])
def bid():
    data = { "$class": "org.itken.Bidding",
             "constructionInformation": request.form['constructionInformation'],
             "company": request.form['company'],
             "bidAmount": request.form['bidAmount'] }
    print(data)
    api.Post("Bidding", data)
    companyName = request.form['companyName']
    return render_template("const/complete.html", companyName=companyName, task="入札")

@app.route("/closeBidding", methods=['POST'])
def closeBidding():
    data = { "$class": "org.itken.CloseBidding", "information": request.form['information'] }
    api.Post("CloseBidding", data)

    return render_template("admin/complete.html", task="落札")

@app.route("/feedback", methods=['POST'])
def feedback():
    data = { "$class": "org.itken.FeedBack",
             "incident": request.form['incident'],
             "satisfaction": {
                 "$class": "org.itken.CustomerSatisfaction",
                 "score": request.form['score']
                 }
             }
    api.Post("FeedBack", data)
    companyName = request.args.get('companyName',  default = '', type = str)

    return render_template("customer/complete.html", task="フィードバック")

@app.route("/const", methods=['GET'])
def const():
    companyName = request.args.get('companyName',  default = '', type = str)
    responses = apiCaller.Const(companyName).responses
    return render_template("const/const.html", companyName=companyName, responses=responses)

@app.route("/const/incidentId", methods=['GET'])
def const_incidentId():
    companyName = request.args.get('companyName',  default = '', type = str)
    incidentId = request.args.get('incidentId',  default = '', type = str)
    const = apiCaller.Const(companyName, incidentId)

    return render_template("const/incidentId.html", companyName=companyName, incidentId=incidentId, data_dict=const.dispData, responses=const.responses)

@app.route("/sonpo", methods=['GET'])
def sonpo():
    responses = apiCaller.Sonpo().responses
    return render_template("sonpo/sonpo.html", responses=responses)

@app.route("/customer", methods=['GET'])
def customer():
    companyName = request.args.get('companyName',  default = '', type = str)
    api = apiCaller.Customer(companyName)
    return render_template("customer/customer.html", companyName=companyName, responses=api.responses, dispData=api.dispData, title="お客様")

@app.route("/customer/incidentId", methods=['GET'])
def customer_incidentId():
    companyName = request.args.get('companyName',  default = '', type = str)
    incidentId = request.args.get('incidentId',  default = '', type = str)
    api = apiCaller.Customer(companyName, incidentId)
    return render_template("customer/incidentId.html", companyName=companyName, incidentId=incidentId, responses=api.responses, dispData=api.dispData, title="お客様")

@app.route("/investigator", methods=['GET'])
def investigator():
    responses = apiCaller.Investigate().responses
    companyName = request.args.get('companyName',  default = '', type = str)
    return render_template("investigate/investigate.html", companyName=companyName, responses=responses)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)


